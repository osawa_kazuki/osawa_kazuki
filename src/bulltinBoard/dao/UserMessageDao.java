package bulltinBoard.dao;

import static bulltinBoard.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import bulltinBoard.beans.UserMessage;
import bulltinBoard.exception.SQLRuntimeException;

public class UserMessageDao {

    public List<UserMessage> getUserMessage(Connection connection, int num, String startDate, String endDate, String category) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("messages.id as id, ");
            sql.append("messages.text as text, ");
            sql.append("messages.subject as subject, ");
            sql.append("messages.category as category, ");
            sql.append("messages.user_id as user_id, ");
            //sql.append("users.login_id as login_id, ");
            sql.append("users.name as name, ");
            sql.append("messages.created_date as created_date ");
            sql.append("FROM messages ");
            sql.append("INNER JOIN users ");
            sql.append("ON messages.user_id = users.id ");
            sql.append("WHERE messages.created_date BETWEEN ? AND ? "); // step1

            // カテゴリー欄が空欄でなかったらsqlにappend
            if(!(StringUtils.isEmpty(category))) {
            	sql.append("AND messages.category LIKE ? ");
                }
                sql.append(" ORDER BY created_date DESC limit " + num);

                ps = connection.prepareStatement(sql.toString());

                // BETWEEN ? = 1 AND ? = 2 日付の範囲
                ps.setString(1, startDate);
                ps.setString(2, endDate);

                 // カテゴリー欄が空欄でなかったら %（ワイルドカード文字）0個以上の文字列で構成される
                if(!(StringUtils.isEmpty(category))) {
                ps.setString(3, "%" + category + "%");
                }

            ResultSet rs = ps.executeQuery();
            List<UserMessage> ret = toUserMessageList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<UserMessage> toUserMessageList(ResultSet rs)
            throws SQLException {

        List<UserMessage> ret = new ArrayList<UserMessage>();
        try {
            while (rs.next()) {
                String name = rs.getString("name");
                int id = rs.getInt("id");
                int user_id = rs.getInt("user_id");
                String text = rs.getString("text");
                String subject = rs.getString("subject");
                String category = rs.getString("category");
                Timestamp createdDate = rs.getTimestamp("created_date");

                UserMessage message = new UserMessage();

                message.setName(name);
                message.setId(id);
                message.setUser_id(user_id);
                message.setText(text);
                message.setSubject(subject);
                message.setCategory(category);
                message.setCreated_date(createdDate);

                ret.add(message);
            }
            return ret;
        } finally {
            close(rs);
        }
    }

}