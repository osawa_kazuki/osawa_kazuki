package bulltinBoard.dao;

import static bulltinBoard.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import bulltinBoard.beans.Message;
import bulltinBoard.exception.SQLRuntimeException;

public class MessageDao {

    public void insert(Connection connection, Message message) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO messages ( ");
            sql.append("user_id");
            sql.append(", subject");
            sql.append(", text");
            sql.append(", category");
            sql.append(", created_date");
            sql.append(") VALUES (");

            sql.append(" ?"); // user_id
            sql.append(", ?"); // subject
            sql.append(", ?"); // text
            sql.append(", ?"); // categoryt
            sql.append(", CURRENT_TIMESTAMP"); // created_date
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setInt(1, message.getUser_id());
            ps.setString(2, message.getSubject());
            ps.setString(3, message.getText());
            ps.setString(4, message.getCategory());

            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    // Delete(DELETE FROM テーブル名 WHERE 条件式);
    public void deleteMessage(Connection connection, Message message) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();

            sql.append("DELETE FROM messages WHERE id = ?");

            ps = connection.prepareStatement(sql.toString());

            ps.setInt(1, message.getId());
            ps.executeUpdate();


        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

}