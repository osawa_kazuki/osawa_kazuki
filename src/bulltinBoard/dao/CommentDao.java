package bulltinBoard.dao;

import static bulltinBoard.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import bulltinBoard.beans.Comment;
import bulltinBoard.exception.SQLRuntimeException;

public class CommentDao {

    public void insert(Connection connection, Comment message) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO comments ( ");
            sql.append("user_id");
            sql.append(", text");
            sql.append(", message_id");
            sql.append(", created_date");
            sql.append(") VALUES (");

            sql.append(" ?"); // user_id
            sql.append(", ?"); // text
            sql.append(", ?"); // message_id
            sql.append(", CURRENT_TIMESTAMP"); // created_date
            sql.append(")");
            ps = connection.prepareStatement(sql.toString());

            ps.setInt(1, message.getUser_id());
            ps.setString(2, message.getText());
            ps.setInt(3, message.getMessage_id());

            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    public List<Comment> getComment(Connection connection, int num) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("comments.id as id, ");
            sql.append("comments.created_date as created_date, ");
            sql.append("comments.text as text, ");
            sql.append("comments.message_id as message_id, ");
            sql.append("comments.user_id as user_id, ");

            sql.append("comments.created_date as created_date ");
            sql.append("FROM comments ");
            sql.append("INNER JOIN users ");
            sql.append("ON comments.user_id = users.id ");
            sql.append("ORDER BY created_date ASC limit " + num);

            ps = connection.prepareStatement(sql.toString());

            System.out.println(ps);

            ResultSet rs = ps.executeQuery();
            List<Comment> ret = toCommentList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    // while分割（ret）

    private List<Comment> toCommentList(ResultSet rs) throws SQLException {

        List<Comment> ret = new ArrayList<Comment>();
        try {
            while (rs.next()) {
                int id = rs.getInt("id");
                String text = rs.getString("text");
                Timestamp created_Date = rs.getTimestamp("created_date");
                int user_id = rs.getInt("user_id");
                int userMessage_id = rs.getInt("message_id");

                Comment comment = new Comment();

                comment.setId(id);
                comment.setText(text);
                comment.setMessage_id(userMessage_id);
                comment.setCreatedDate(created_Date);
                comment.setUserId(user_id);

                ret.add(comment);
            }
            return ret;
        } finally {
            close(rs);
        }
    }

    // Delete(DELETE FROM テーブル名 WHERE 条件式);
    public void deleteComment(Connection connection, Comment commentId) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();

            sql.append("DELETE FROM comments WHERE id = ?");

            ps = connection.prepareStatement(sql.toString());

            ps.setInt(1, commentId.getId());
            ps.executeUpdate();


        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

}