package bulltinBoard.dao;

import static bulltinBoard.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import bulltinBoard.beans.Branch;
import bulltinBoard.exception.SQLRuntimeException;

public class BranchDao {

    public List<Branch> adminBranch(Connection connection, int num) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT * FROM branches ");

            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<Branch> ret = adminBranchList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
    private List<Branch> adminBranchList(ResultSet rs)
            throws SQLException {

        List<Branch> ret = new ArrayList<Branch>();
        try {
            while (rs.next()) {

            	int id = rs.getInt("id");
                String name = rs.getString("name");

                Branch branch = new Branch();

                branch.setId(id);
                branch.setName(name);

                ret.add(branch);
                // System.out.println(ret);

            }
            return ret;
        } finally {
            close(rs);
        }
    }
}