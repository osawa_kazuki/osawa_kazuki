package bulltinBoard.dao;

import static bulltinBoard.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import bulltinBoard.beans.User;
import bulltinBoard.exception.SQLRuntimeException;

public class UserDao {

	// INSERT（データの登録処理）
    public void insert(Connection connection, User user) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO users ( ");
            sql.append("login_id");
            sql.append(", name");
            sql.append(", password");
            sql.append(", branch_id");
            sql.append(", position_id");
            sql.append(", is_stopped");
            sql.append(", created_date");
            sql.append(", updated_date");
            sql.append(") VALUES (");

            sql.append("?"); // login_id
            sql.append(", ?"); // name
            sql.append(", ?"); // password
            sql.append(", ?"); // branch_id
            sql.append(", ?"); // position_id
            sql.append(", 0"); // is_stopped → 予め0を渡して可動状態に
            sql.append(", CURRENT_TIMESTAMP"); // created_date
            sql.append(", CURRENT_TIMESTAMP"); // updated_date
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, user.getLogin_id());
            ps.setString(2, user.getName());
            ps.setString(3, user.getPassword());
            ps.setInt(4, user.getBranch_id());
            ps.setInt(5, user.getPosition_id());
            // ps.setInt(6, user.getIs_stopped());

            // executeUpdate（指定されたSQL文を実行処理）
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    // getUSer
    public User getUser(Connection connection, String login_id, String password) {

        PreparedStatement ps = null;
        try {
            String sql = "SELECT * FROM users WHERE (login_id = ? AND password = ?)";

            ps = connection.prepareStatement(sql);
            ps.setString(1, login_id);
            ps.setString(2, password);

            ResultSet rs = ps.executeQuery();
            List<User> userList = toUserList(rs);
            if (userList.isEmpty() == true) {
                return null;
            } else if (2 <= userList.size()) {
                throw new IllegalStateException("2 <= userList.size()");
            } else {
                return userList.get(0);
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    // userList
    private List<User> toUserList(ResultSet rs) throws SQLException {

        List<User> ret = new ArrayList<User>();
        try {
            while (rs.next()) {
                int id = rs.getInt("id");
                String login_id = rs.getString("login_id");
                String name = rs.getString("name");
                int branch_id = rs.getInt("branch_id");
                int position_id = rs.getInt("position_id");
                int is_stopped = rs.getInt("is_stopped");
                Timestamp createdDate = rs.getTimestamp("created_date");

                User user = new User();
                user.setId(id);
                user.setLogin_id(login_id);
                user.setName(name);
                user.setBranch_id(branch_id);
                user.setPosition_id(position_id);
                user.setIs_stopped(is_stopped);
                user.setCreatedDate(createdDate);

                ret.add(user);
            }
            return ret;
        } finally {
            close(rs);
        }
    }

    // adminUser
    public List<User> adminUser(Connection connection, int num) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT * FROM users ");
            sql.append("ORDER BY created_date DESC limit " + num);

            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<User> ret = adminUserList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    // adminUserList
    private List<User> adminUserList(ResultSet rs) throws SQLException {

        List<User> ret = new ArrayList<User>();
        try {
            while (rs.next()) {

                int id = rs.getInt("id");
                String login_id = rs.getString("login_id");
                String name = rs.getString("name");
                int branch_id = rs.getInt("branch_id");
                int position_id = rs.getInt("position_id");
                int is_stopped = rs.getInt("is_stopped");
                Timestamp createdDate = rs.getTimestamp("created_date");

                User user = new User();

                user.setId(id);
                user.setLogin_id(login_id);
                user.setName(name);
                user.setBranch_id(branch_id);
                user.setPosition_id(position_id);
                user.setIs_stopped(is_stopped);
                user.setCreatedDate(createdDate);

                ret.add(user);
                // System.out.println(ret);
            }
            return ret;
        } finally {
            close(rs);
        }
    }

    // 復活停止
    public void is_stoppedDao(Connection connection, int user_id, int is_stopped) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();

            if(is_stopped == 0) {
            sql.append("UPDATE users SET ");
            sql.append("is_stopped = 1 ");
            sql.append("WHERE ");
            sql.append("id = ?");

            ps = connection.prepareStatement(sql.toString());
            ps.setInt(1, user_id);

            // System.out.println(ps);

            ps.executeUpdate();

            } else {
        	sql.append("UPDATE users SET ");
            sql.append("is_stopped = 0 ");
            sql.append("WHERE ");
            sql.append("id = ?");

            ps = connection.prepareStatement(sql.toString());
            ps.setInt(1, user_id);

            System.out.println(ps);

            ps.executeUpdate();

            }

        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    // settings
    public User settings(Connection connection, int userSettings) {

    	PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();

            // sql.append("SELECT FROM messages WHERE (login_id = ?) AND NOT id = ?");

            sql.append("SELECT * FROM users ");
            sql.append("WHERE ");
            sql.append("id = ?");

            ps = connection.prepareStatement(sql.toString());
            ps.setInt(1, userSettings);

            ResultSet rs = ps.executeQuery();
            List<User> userList = toUserList(rs);

            if (userList.isEmpty() == true) {
                return null;
            } else if (2 <= userList.size()) {
                throw new IllegalStateException("2 <= userList.size()");
            } else {
                return userList.get(0);
            }

        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    // updateUser
    public void updateUser(Connection connection, User updateUser, int user_id) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();

            sql.append("UPDATE users SET ");
            sql.append("name = ? ");
            sql.append(",login_id = ? ");

            if(!StringUtils.isEmpty(updateUser.getPassword()) == true) {
             sql.append(",password = ? ");
            }

            sql.append(",branch_id = ? ");
            sql.append(",position_id = ? ");

            sql.append("WHERE ");
            sql.append("id = ?");

            ps = connection.prepareStatement(sql.toString());
            ps.setString(1, updateUser.getName());
            ps.setString(2, updateUser.getLogin_id());

            if(!(StringUtils.isEmpty(updateUser.getPassword())) == true) {
            ps.setString(3, updateUser.getPassword());
            ps.setInt(4, updateUser.getBranch_id());
            ps.setInt(5, updateUser.getPosition_id());
            ps.setInt(6, user_id);
            } else {
            	ps.setInt(3, updateUser.getBranch_id());
                ps.setInt(4, updateUser.getPosition_id());
                ps.setInt(5, user_id);
            }

            // SQL実行
            ps.executeUpdate();

        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    // 重複ユーザー（ログインIDで判断）
    public User duplicate(Connection connection, String login_id) {

        PreparedStatement ps = null;
            try {
                StringBuilder sql = new StringBuilder();
                sql.append("SELECT * FROM users ");
                sql.append("WHERE ");
                sql.append("login_id = ?");

                ps = connection.prepareStatement(sql.toString());
                ps.setString(1, login_id);

                ResultSet rs = ps.executeQuery();
                List<User> login_idList = toLogin_idList(rs);

                if (login_idList.isEmpty() == true) {
                    return null;
                } else if (2 <= login_idList.size()) {
                    throw new IllegalStateException("2 <= userList.size()");
                } else {
                    return login_idList.get(0);
                }

            } catch (SQLException e) {
                throw new SQLRuntimeException(e);
            } finally {
                close(ps);
            }
        }

    private List<User> toLogin_idList(ResultSet rs) throws SQLException {

        List<User> ret = new ArrayList<User>();
        try {
            while (rs.next()) {
                int id = rs.getInt("id");
                String login_id = rs.getString("login_id");

                User user = new User();
                user.setId(id);
                user.setLogin_id(login_id);

                ret.add(user);
            }
            return ret;
        } finally {
            close(rs);
        }
    }
}

