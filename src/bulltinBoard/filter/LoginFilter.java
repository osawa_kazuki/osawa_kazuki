package bulltinBoard.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import bulltinBoard.beans.User;

@WebFilter("/*")

public class LoginFilter implements Filter {

	@Override
	  public void doFilter(ServletRequest request, ServletResponse response,
			  FilterChain chain) throws IOException, ServletException {

	      String target = ((HttpServletRequest)request).getServletPath();
	      HttpSession session = ((HttpServletRequest)request).getSession();
	      User Loginuser = (User)session.getAttribute("loginUser");

	      List<String> messages = new ArrayList<String>();

	      // Loginuserがnullで、targetとloginuserが同じでないとredirect
	      if (Loginuser == null) {
	    	 if (!(target.equals("/login"))) {
	    			 if (!(target.contains("/css"))) {
	    		messages.add("ログインしてください");
	    		session.setAttribute("errorMessages", messages);
	    		 ((HttpServletResponse)response).sendRedirect("login");
	    		 return; // returnしないとdoFilterも起動してしまう
	    			 }
	         }
	      }

	      chain.doFilter(request, response); // サーブレットを実行
	  }

	  @Override
	  public void init(FilterConfig config) {

	  }

	  @Override
	  public void destroy() {

	  }
}
