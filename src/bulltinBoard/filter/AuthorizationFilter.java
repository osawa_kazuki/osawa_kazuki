package bulltinBoard.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import bulltinBoard.beans.User;

@WebFilter(urlPatterns = {"/admin", "/signup", "/settings"})

public class AuthorizationFilter implements Filter {

	@Override
	  public void doFilter(ServletRequest request, ServletResponse response,
			  FilterChain chain) throws IOException, ServletException {

	      String target = ((HttpServletRequest)request).getServletPath();
	      HttpSession session = ((HttpServletRequest)request).getSession();
	      User Loginuser = (User)session.getAttribute("loginUser");

	      List<String> messages = new ArrayList<String>();

	      // 本社（branch_id == 1）であり総務・情報（position == 1 && 2）以外はredirect
	      if(!(Loginuser == null)) {
	          if (!(target.equals("/login"))) {
		          if (!(Loginuser.getBranch_id() == 1 && Loginuser.getPosition_id() == 1 || Loginuser.getPosition_id() == 2)) {
		    	      messages.add("アクセス権限がありません");
		    	      session.setAttribute("errorMessages", messages);
		    	      ((HttpServletResponse)response).sendRedirect("./");
		    	      return; // returnしないとdoFilterも起動してしまう
		          }
	          }
	      }

	      chain.doFilter(request, response); // サーブレットを実行
	  }

	  @Override
	  public void init(FilterConfig config) {

	  }

	  @Override
	  public void destroy() {

	  }
}
