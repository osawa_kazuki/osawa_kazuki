package bulltinBoard.service;

import static bulltinBoard.utils.CloseableUtil.*;
import static bulltinBoard.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import bulltinBoard.beans.Comment;
import bulltinBoard.dao.CommentDao;

public class CommentService {

    public void register(Comment message) {

        Connection connection = null;
        try {
            connection = getConnection();

            CommentDao messageDao = new CommentDao();
            messageDao.insert(connection, message);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    private static final int LIMIT_NUM = 1000;

    public List<Comment> getComment() {

    	Connection connection = null;
    	try {
    		connection = getConnection();

    		CommentDao commentDao = new CommentDao();
    		List<Comment> ret = commentDao.getComment(connection, LIMIT_NUM);

    		commit(connection);

    		return ret;
    	} catch (RuntimeException e) {
    		rollback(connection);
    		throw e;
    	} catch (Error e) {
    		rollback(connection);
    		throw e;
    	} finally {
    		close(connection);
    	}
    }

    public void deleteComment(Comment commentId) {

   	 Connection connection = null;
        try {
            connection = getConnection();

            CommentDao deleteCommentDao = new CommentDao();
            deleteCommentDao.deleteComment(connection, commentId);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

}
