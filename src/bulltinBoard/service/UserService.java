package bulltinBoard.service;

import static bulltinBoard.utils.CloseableUtil.*;
import static bulltinBoard.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import bulltinBoard.beans.User;
import bulltinBoard.dao.UserDao;
import bulltinBoard.utils.CipherUtil;

public class UserService {

    public void register(User user) {

        Connection connection = null;
        try {
            connection = getConnection();

            String encPassword = CipherUtil.encrypt(user.getPassword());
            user.setPassword(encPassword);

            UserDao userDao = new UserDao();
            userDao.insert(connection, user);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    private static final int LIMIT_NUM = 1000;
    public List<User> adminUser() {

        Connection connection = null;
        try {
            connection = getConnection();

            UserDao adminUserDao = new UserDao();
            List<User> ret = adminUserDao.adminUser(connection, LIMIT_NUM);

            commit(connection);

            return ret;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public void is_stopped(int user_id, int is_stopped) {

        Connection connection = null;
            try {
                connection = getConnection();

                UserDao is_stoppedDao = new UserDao();
                is_stoppedDao.is_stoppedDao(connection, user_id, is_stopped);

                commit(connection);
            } catch (RuntimeException e) {
                rollback(connection);
                throw e;
            } catch (Error e) {
                rollback(connection);
                throw e;
            } finally {
                close(connection);
            }
        }

    public User settings(int userSettings) {

        Connection connection = null;
        try {
            connection = getConnection();

            UserDao settingsDao = new UserDao();
            User ret = settingsDao.settings(connection, userSettings);

            commit(connection);

            return ret;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public void update(User updateUser, int user_id) {

        Connection connection = null;
            try {
                connection = getConnection();

                if(!StringUtils.isEmpty(updateUser.getPassword()) == true) {
                	String encPassword = CipherUtil.encrypt(updateUser.getPassword());
                	updateUser.setPassword(encPassword);
                }

                UserDao is_stoppedDao = new UserDao();
                is_stoppedDao.updateUser(connection, updateUser,  user_id);

                commit(connection);
            } catch (RuntimeException e) {
                rollback(connection);
                throw e;
            } catch (Error e) {
                rollback(connection);
                throw e;
            } finally {
                close(connection);
            }
        }

        // duplicate 重複ユーザー用
        public User duplicate(String login_id) {

            Connection connection = null;
            try {
                connection = getConnection();
                UserDao userDao = new UserDao();
                User user = userDao.duplicate(connection, login_id);

                commit(connection);

                return user;
            } catch (RuntimeException e) {
                rollback(connection);
                throw e;
            } catch (Error e) {
                rollback(connection);
                throw e;
            } finally {
                close(connection);
        }
    }
}
