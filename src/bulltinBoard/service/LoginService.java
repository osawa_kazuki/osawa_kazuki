package bulltinBoard.service;

import static bulltinBoard.utils.CloseableUtil.*;
import static bulltinBoard.utils.DBUtil.*;

import java.sql.Connection;

import bulltinBoard.beans.User;
import bulltinBoard.dao.UserDao;
import bulltinBoard.utils.CipherUtil;

public class LoginService {

    public User login(String login_id, String password) {

        Connection connection = null;
        try {
            connection = getConnection();

            UserDao userDao = new UserDao();
            String encPassword = CipherUtil.encrypt(password);
            User user = userDao.getUser(connection, login_id, encPassword);

            commit(connection);

            return user;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

}