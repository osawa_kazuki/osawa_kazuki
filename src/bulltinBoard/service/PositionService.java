package bulltinBoard.service;

import static bulltinBoard.utils.CloseableUtil.*;
import static bulltinBoard.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import bulltinBoard.beans.Position;
import bulltinBoard.dao.PositionDao;

public class PositionService {

    private static final int LIMIT_NUM = 1000;
    public List<Position> adminPosition() {

        Connection connection = null;
        try {
            connection = getConnection();

            PositionDao adminPositionDao = new PositionDao();
            List<Position> ret = adminPositionDao.adminPosition(connection, LIMIT_NUM);

            commit(connection);

            return ret;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
}