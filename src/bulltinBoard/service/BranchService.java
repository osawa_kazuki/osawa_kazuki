package bulltinBoard.service;

import static bulltinBoard.utils.CloseableUtil.*;
import static bulltinBoard.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import bulltinBoard.beans.Branch;
import bulltinBoard.dao.BranchDao;

public class BranchService {

    private static final int LIMIT_NUM = 1000;
    public List<Branch> adminBranch() {

        Connection connection = null;
        try {
            connection = getConnection();

            BranchDao adminBranchDao = new BranchDao();
            List<Branch> ret = adminBranchDao.adminBranch(connection, LIMIT_NUM);

            commit(connection);

            return ret;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
}