package bulltinBoard.service;

import static bulltinBoard.utils.CloseableUtil.*;
import static bulltinBoard.utils.DBUtil.*;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import bulltinBoard.beans.Message;
import bulltinBoard.beans.UserMessage;
import bulltinBoard.dao.MessageDao;
import bulltinBoard.dao.UserMessageDao;

public class MessageService {

    public void register(Message message) {

        Connection connection = null;
        try {
            connection = getConnection();

            MessageDao messageDao = new MessageDao();
            messageDao.insert(connection, message);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    private static final int LIMIT_NUM = 1000;

    public List<UserMessage> getMessage(String start, String end, String category) {

    	Connection connection = null;
    	try {
    		// yyyy = 西暦 MM = 月 dd = 日 HH = 時間 mm = 分 ss = 秒
    		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        	Date date = new Date();
        	connection = getConnection();

        	String startDate = null;
        	String endDate = null;

            // StringUtils.isEmpty : nullか空文字のときtrueを返す
            if(StringUtils.isEmpty(start)) {
            	 startDate = "2019-01-01 00:00:00"; // default step2
            }else {
            	 startDate = start + " 00:00:00";
            }

            // StringUtils.isEmpty : nullか空文字のときtrueを返す
            if(StringUtils.isEmpty(end)) {
            	 endDate = sdf.format(date); // 引数の日付をフォーマットする
            }else {
            	 endDate = end + " 23:59:59";
            }

    		UserMessageDao messageDao = new UserMessageDao();
    		List<UserMessage> ret = messageDao.getUserMessage(connection, LIMIT_NUM, startDate, endDate, category);

    		commit(connection);

    		return ret;
    	} catch (RuntimeException e) {
    		rollback(connection);
    		throw e;
    	} catch (Error e) {
    		rollback(connection);
    		throw e;
    	} finally {
    		close(connection);
    	}
    }

    public void deleteMessage(Message message) {

        Connection connection = null;
        try {
            connection = getConnection();

            MessageDao deleteMessageDao = new MessageDao();
            deleteMessageDao.deleteMessage(connection, message);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
}
