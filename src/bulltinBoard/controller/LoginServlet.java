package bulltinBoard.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import bulltinBoard.beans.User;
import bulltinBoard.service.LoginService;

@WebServlet(urlPatterns = { "/login" })
public class LoginServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        request.getRequestDispatcher("login.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        String login_id = request.getParameter("login_id");
        String password = request.getParameter("password");

        LoginService loginService = new LoginService();
        User user = loginService.login(login_id, password);

        HttpSession session = request.getSession();
        if (user != null && user.getIs_stopped() == 0) {

			session.setAttribute("loginUser", user);
			response.sendRedirect("./");
			return;

        } else {
            List<String> messages = new ArrayList<String>();
            // ログインボタンが押下されたとき
            if(StringUtils.isBlank(login_id) || StringUtils.isBlank(password)) {
				messages.add("ログインIDまたはパスワードが入力されていません。");
				session.setAttribute("errorMessages", messages);
            } else {
            	messages.add("ログインIDまたはパスワードが誤っています。");
    			session.setAttribute("errorMessages", messages);
    	    }

            request.setAttribute("login_id", login_id);
            request.getRequestDispatcher("login.jsp").forward(request, response);
            //response.sendRedirect("login");
        }
    }

}