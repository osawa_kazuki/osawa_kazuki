package bulltinBoard.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bulltinBoard.beans.Branch;
import bulltinBoard.beans.Position;
import bulltinBoard.beans.User;
import bulltinBoard.service.BranchService;
import bulltinBoard.service.PositionService;
import bulltinBoard.service.UserService;

@WebServlet(urlPatterns = { "/admin" })
public class AdminServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        List<User> users = new UserService().adminUser();
        List<Branch> branches = new BranchService().adminBranch();
        List<Position> positions = new PositionService().adminPosition();

        request.setAttribute("users", users);
        request.setAttribute("branches", branches);
        request.setAttribute("positions", positions);

        request.getRequestDispatcher("/admin.jsp").forward(request, response);
    }
}