package bulltinBoard.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import bulltinBoard.beans.Message;
import bulltinBoard.service.MessageService;

@WebServlet(urlPatterns = { "/DELETEMessage" })
public class DeleteNewMessageServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override

    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {
    		request.getRequestDispatcher("/top.jsp").forward(request, response);
    }

    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        HttpSession session = request.getSession();

        List<String> messages = new ArrayList<String>();

        if (isValid(request, messages) == true) {

            Message message = new Message();
            message.setId(Integer.parseInt(request.getParameter("message_id")));

            // System.out.println(message);

            new MessageService().deleteMessage(message);

            response.sendRedirect("./");
        } else {
            session.setAttribute("errorMessages", messages);
            response.sendRedirect("./");
        }
    }

    private boolean isValid(HttpServletRequest request, List<String> messages) {

        // String message = request.getParameter("category");
        // String message = request.getParameter("subject");
        // String message = request.getParameter("newMessage");

        // if (StringUtils.isEmpty(message) == true) {
        //     messages.add("メッセージを入力してください");
        // }
        // if (140 < message.length()) {
        //     messages.add("140文字以下で入力してください");
        // }
        // if (messages.size() == 0) {
             return true;
        // } else {
        //     return false;
        // }
    }

}