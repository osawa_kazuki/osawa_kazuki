package bulltinBoard.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import bulltinBoard.beans.Message;
import bulltinBoard.beans.User;
import bulltinBoard.service.MessageService;

@WebServlet(urlPatterns = { "/newmessage" })
public class NewMessageServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

    		request.getRequestDispatcher("/newmessage.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        HttpSession session = request.getSession();

        List<String> messages = new ArrayList<String>();

        if (isValid(request, messages) == true) {

            User user = (User) session.getAttribute("loginUser");

            Message message = new Message();
            message.setCategory(request.getParameter("category"));
            message.setSubject(request.getParameter("subject"));
            message.setText(request.getParameter("text"));
            message.setUser_id(user.getId());

            new MessageService().register(message);

            response.sendRedirect("./");
        } else {

        	 String category = request.getParameter("category");
             String subject = request.getParameter("subject");
             String text = request.getParameter("text");

             request.setAttribute("category", category);
        	 request.setAttribute("subject", subject);
        	 request.setAttribute("text", text);

             session.setAttribute("errorMessages", messages);
             request.getRequestDispatcher("newmessage.jsp").forward(request, response);
             // response.sendRedirect("newmessage");
        }
    }

    private boolean isValid(HttpServletRequest request, List<String> messages) {

        String category = request.getParameter("category");
        String subject = request.getParameter("subject");
        String text = request.getParameter("text");

        if (StringUtils.isBlank(category) == true) {
            messages.add("カテゴリーを入力してください");
        }
        if (!(StringUtils.isBlank(category) == true)) {
            if (10 < category.length()) {
            messages.add("カテゴリーは10文字以下で入力してください");
            }
        }
        if (StringUtils.isBlank(subject) == true) {
            messages.add("件名を入力してください");
        }
        if (!(StringUtils.isBlank(subject) == true)) {
            if (30 < subject.length()) {
            messages.add("件名は30文字以下で入力してください");
            }
        }
        if (StringUtils.isBlank(text) == true) {
            messages.add("本文を入力してください");
        }
        if (!(StringUtils.isBlank(text) == true)) {
            if (1000 < text.length()) {
            messages.add("本文は1000文字以下で入力してください");
            }
        }
        if (messages.size() == 0) {
            return true;
        } else {
        	 // request.setAttribute("category", category);
        	 // request.setAttribute("subject", subject);
        	 // request.setAttribute("text", text);
            return false;
        }
    }

}
