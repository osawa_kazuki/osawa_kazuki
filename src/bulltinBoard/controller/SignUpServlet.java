package bulltinBoard.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import bulltinBoard.beans.Branch;
import bulltinBoard.beans.Position;
import bulltinBoard.beans.User;
import bulltinBoard.service.BranchService;
import bulltinBoard.service.PositionService;
import bulltinBoard.service.UserService;

@WebServlet(urlPatterns = { "/signup" })
public class SignUpServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

    	// Branch型のリスト・Position型のリストを作成、それぞれadminにある支店名・部署名を参照
    	List<Branch> branches = new BranchService().adminBranch();
    	List<Position> positions = new PositionService().adminPosition();

    	request.setAttribute("branches", branches);
    	request.setAttribute("positions", positions);

    	request.getRequestDispatcher("/signup.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        List<String> messages = new ArrayList<String>();
    	List<Branch> branches = new BranchService().adminBranch();
    	List<Position> positions = new PositionService().adminPosition();

    	// セッションに入れる
        HttpSession session = request.getSession();

        if (isValid(request, messages) == true) {

            User user = new User();

            // 文字列（String）を数値に変換（branch_id、position_id）
            user.setName(request.getParameter("name"));
            user.setLogin_id(request.getParameter("login_id"));
            user.setPassword(request.getParameter("password"));
            user.setBranch_id(Integer.parseInt(request.getParameter("branch_id")));
            user.setPosition_id(Integer.parseInt(request.getParameter("position_id")));

            new UserService().register(user);

            response.sendRedirect("admin");
        } else {
        	String name = request.getParameter("name");
        	String login_id = request.getParameter("login_id");
        	String validBranch_id = request.getParameter("branch_id");
    	    String validPosition_id = request.getParameter("position_id");

    	    request.setAttribute("name", name);
	       	request.setAttribute("login_id", login_id);

	       	// 支店コード、支店名
	       	request.setAttribute("validBranch_id", validBranch_id);
	       	request.setAttribute("branches", branches);

	       	// 部署・役職コード、部署・役職名
	       	request.setAttribute("validPosition_id",validPosition_id);
	       	request.setAttribute("positions", positions);

            session.setAttribute("errorMessages", messages);
            // response.sendRedirect("signup");
            request.getRequestDispatcher("signup.jsp").forward(request, response);
        }
    }

    private boolean isValid(HttpServletRequest request, List<String> messages) {
    	String name = request.getParameter("name");
        String login_id = request.getParameter("login_id");
        String password = request.getParameter("password");
        String checkPassword = request.getParameter("checkPassword");
        int branch_id = Integer.parseInt(request.getParameter("branch_id"));
    	int position_id = Integer.parseInt(request.getParameter("position_id"));

        User user = new UserService().duplicate(login_id);

        if (StringUtils.isBlank(name) == true) {
            messages.add("名前を入力してください");
        }
        if (!(StringUtils.isBlank(name) == true)) {
            if (10 < name.length()) {
            messages.add("名前は10文字以下で入力してください");
            }
        }
        if (user != null) {
        	messages.add("既にログインIDが存在します");
        }
        if (StringUtils.isBlank(login_id) == true) {
            messages.add("ログインIDを入力してください");
        }
        if (!(StringUtils.isBlank(login_id))) {
            if (!(login_id.matches("[!-~]{6,20}" ))) { // ex 6文字以上、10文字以下の半角英数字 [!-~]{6,10}
        	messages.add("ログインIDは半角英数字で6文字以上20文字以下で入力してください");
            }
        }
        if (StringUtils.isBlank(password) == true) {
            messages.add("パスワードを入力してください");
        }
        if (!(StringUtils.isBlank(password))) {
	        if (!(password.matches("[!-~]{6,20}" ))) {
	        	messages.add("パスワードは半角英数字で6文字以上20文字以下で入力してください");
	        }
        }
        // if (StringUtils.isEmpty(checkPassword) == true) {
        //    messages.add("確認用パスワードを入力してください");
        // }
        if (!(password.equals(checkPassword))) {
        	messages.add("パスワードとパスワード（確認）が一致しません");
        }
        if (branch_id == 0 || position_id == 0) {
        	messages.add("支店または部署・役職を入力してください");
        }
        if ((branch_id == 1 && position_id == 3) || (branch_id == 1 && position_id == 4)) {
        	messages.add("支店と部署・役職の組み合わせが不正です");
        }
        if ((branch_id == 2 && position_id == 1) || (branch_id == 2 && position_id == 2)) {
        	messages.add("支店と部署・役職の組み合わせが不正です");
        }
        if ((branch_id == 3 && position_id == 1) || (branch_id == 3 && position_id == 2)) {
        	messages.add("支店と部署・役職の組み合わせが不正です");
        }
        if ((branch_id == 4 && position_id == 1) || (branch_id == 4 && position_id == 2)) {
        	messages.add("支店と部署・役職の組み合わせが不正です");
        }
        // TODO アカウントが既に利用されていないか、メールアドレスが既に登録されていないかなどの確認も必要
        if (messages.size() == 0) {
            return true;
        } else {
        	 // request.setAttribute("name", name);
        	 // request.setAttribute("login_id", login_id);
        	 // request.setAttribute("branch_Id", branch_id);
        	 // request.setAttribute("position_Id", position_id);
            return false;
        }
    }

}