package bulltinBoard.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import bulltinBoard.service.UserService;

@WebServlet(urlPatterns = { "/IsStopped" })
public class IsStoppedServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        HttpSession session = request.getSession();
        List<String> comments = new ArrayList<String>();

        if (isValid(request, comments) == true) {

            int user_id = Integer.parseInt(request.getParameter("user_id"));
            int is_stopped = Integer.parseInt(request.getParameter("is_stopped"));

            System.out.println(user_id);

            new UserService().is_stopped(user_id, is_stopped);

            response.sendRedirect("admin");
        } else {
            session.setAttribute("errorMessages", comments);
            response.sendRedirect("admin");
        }
    }

    private boolean isValid(HttpServletRequest request, List<String> comments) {

        // String message = request.getParameter("category");
        // String message = request.getParameter("subject");
        // String message = request.getParameter("newmessage");

        // if (StringUtils.isEmpty(message) == true) {
        //    messages.add("メッセージを入力してください");
        // }
        // if (140 < message.length()) {
        //    messages.add("140文字以下で入力してください");
        // }
        if (comments.size() == 0) {
            return true;
        } else {
            return false;
        }
    }
}