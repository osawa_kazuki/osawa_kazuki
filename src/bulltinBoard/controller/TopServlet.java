package bulltinBoard.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bulltinBoard.beans.Comment;
import bulltinBoard.beans.UserMessage;
import bulltinBoard.service.CommentService;
import bulltinBoard.service.MessageService;

@WebServlet(urlPatterns = { "/index.jsp" })
public class TopServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

    	// 絞り込み機能用
    	String start = request.getParameter("start");
    	String end = request.getParameter("end");
    	String category = request.getParameter("category");

    	// 投稿に関する情報（開始日時・終了日時・カテゴリー）をリスト化
        List<UserMessage> messages = new MessageService().getMessage(start, end, category);
        List<Comment> comments = new CommentService().getComment();

        request.setAttribute("start", start);
        request.setAttribute("end", end);
        request.setAttribute("category", category);

        request.setAttribute("messages", messages);
        request.setAttribute("comments", comments);
        // クライアントからリクエストを受け取り、さらにそのリクエストをサーバ上のリソースに転送(top.jsp)
        request.getRequestDispatcher("/top.jsp").forward(request, response);
    }
}