package bulltinBoard.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import bulltinBoard.beans.Comment;
import bulltinBoard.beans.User;
import bulltinBoard.service.CommentService;

@WebServlet(urlPatterns = { "/newComment" })
public class NewCommentServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    /*@Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

    	 request.getRequestDispatcher("top.jsp").forward(request, response);
    }*/

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        HttpSession session = request.getSession();

        List<String> comments = new ArrayList<String>();

        if (isValid(request, comments) == true) {

            User user = (User) session.getAttribute("loginUser");

            Comment comment = new Comment();
            comment.setText(request.getParameter("text"));
            comment.setUserId(user.getId());
            comment.setMessage_id(Integer.parseInt(request.getParameter("message_id")));

            new CommentService().register(comment);

            response.sendRedirect("./");
        } else {
            session.setAttribute("errorMessages", comments);
            response.sendRedirect("./");
        }
    }

    private boolean isValid(HttpServletRequest request, List<String> comments) {

        String comment = request.getParameter("text");

        if (StringUtils.isBlank(comment) == true) {
        	comments.add("コメントを入力してください");
        }
        if (!(StringUtils.isBlank(comment) == true)) {
            if (500 < comment.length()) {
        	comments.add("コメントは500文字以下で入力してください");
            }
        }
        if (comments.size() == 0) {
            return true;
        } else {
            return false;
        }
    }

}