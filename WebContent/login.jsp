<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>ログイン</title>
         <link href="./css/style.css" rel="stylesheet" type="text/css">

    </head>

    <body>

     <div class="main-contents">
     <div class="header">

            <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="message">
                            <li><c:out value="${message}" />
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorMessages" scope="session"/>
            </c:if>

       </div>
       </div>

       <div class="login">
       <div style="padding: 10px; margin-bottom: 10px; border: 1px solid #333333; border-radius: 10px;">

            <form action="login" method="post"><br />
            <p>
                <label for="login_id">ログインID</label><br />
                <input name="login_id" id="login_id" value="${login_id}" /><br />
                <label for="password">パスワード</label><br />
                <input name="password" type="password" id="password" /><br /><br />

                <input type="submit" value="ログイン" /> <br />
            </p>
            </form>
            <div class="copyright"> Copyright(c)KazukiOsawa</div>
      </div>
      </div>

    </body>
</html>