<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>ユーザー編集</title>
        <link href="./css/style.css" rel="stylesheet" type="text/css">
    </head>

    <body>

        <div class="main-contents">
        <div class="header">

        <br><h3>ユーザー編集</h3>

            <c:if test="${ not empty errorMessages }">
            <div class="errorMessages">
            <ul>
             <c:forEach items="${errorMessages}" var="message">
             <li><c:out value="${message}" />
             </c:forEach>
            </ul>
            </div>
            <c:remove var="errorMessages" scope="session"/>
            </c:if>

        </div>
        </div>

        <div class="users">

	        <form action="settings" method="post">

                <div class="user">
                    <div class="login_id-name">
               	        <span class="name">名前<br />
               	            <input name="name" id="name" value="${settingsUsers.name}"/></span> <br />
                        <span class="login_id">ログインID<br />
                            <input name="login_id" id="login_id" value="${settingsUsers.login_id}"/></span> <br />
                        <span class="password">パスワード<br />
                            <input name="password" type="password" id="password" /></span> <br />
                        <span class="checkPassword">パスワード（確認）<br />
                            <input name="checkPassword" type="password" id="password" /></span> <br />

                        <c:if test="${loginUser.id != settingsUsers.id}">

       				        <div class="branches">
                            <div class="branch">
				            <div class="login_id-name">

		                    <span class="name">支店名<c:out value="${branch.name}" /></span><br />
		                    <select name="branch_id">
		                        <option value="0">選択してください</option>
		                		<c:forEach items="${branches}" var="branch">
		                		<c:if test="${branch.id == settingsUsers.branch_id}">
								  <option  value="${branch.id}" selected>"${branch.name}"</option>
							    </c:if>

								<c:if test="${branch.id != settingsUsers.branch_id}">
							      <option  value="${branch.id}" >${branch.name}</option>
							    </c:if>
								</c:forEach>
							</select><br />

				            </div>
				            </div>
				  		    </div>
                         </c:if>

                         <c:if test="${loginUser.id != settingsUsers.id}">
				            <div class="positions">
  				            <div class="position">
				            <div class="login_id-name">

	                        <span class="name">部署・役職<c:out value="${position.name}" /></span><br />
	                    	<select name="position_id">
		                      <option value="0">選択してください</option>
		                	  <c:forEach items="${positions}" var="position">
		                	  <c:if test="${position.id == settingsUsers.position_id}">
						        <option  value="${position.id}" selected>"${position.name}"</option>
						      </c:if>

							  <c:if test="${position.id != settingsUsers.position_id}">
							    <option  value="${position.id}">${position.name}</option>
							  </c:if>
							  </c:forEach>
						    </select><br />

				            </div>
				            </div>
				            </div>
				        </c:if>

				        <c:if test="${loginUser.id == settingsUsers.id}">
				        <input type="hidden" name="branch_id" value="${settingsUsers.branch_id}" >
					    <input type="hidden" name="position_id" value="${settingsUsers.position_id}">
				        </c:if>

       				    <br /> <input type="hidden" name="settings" value="${settingsUsers.id}">
                        <input type="submit" value="更新" class="button1" /> <br /> <br />
              		    <a href="admin">戻る</a> <br />

			        </div>
                </div>
		    </form>

        </div>
    </body>
</html>