<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>新規投稿</title>
        <link href="./css/style.css" rel="stylesheet" type="text/css">
    </head>

    <body>

        <div class="main-contents">
        <div class="header">

        <br><h3>新規投稿</h3>

        <c:if test="${ not empty errorMessages }">
            <div class="errorMessages">
            <ul>
            <c:forEach items="${errorMessages}" var="message">
                <li><c:out value="${message}" />
            </c:forEach>
            </ul>
            </div>
            <c:remove var="errorMessages" scope="session"/>
        </c:if>

        </div>
        </div>

        <div class="form-area">

		   	    <form action="newmessage" method="post">
		        カテゴリー
		            <br />
		            <!-- <label for="category"> </label>
		            <textarea name="category" id="category" cols="20" rows="1" class="tweet-box" ><c:out value="${category}" /></textarea> -->
		            <label for="category"></label> <input name="category" value="${category}" id="category" placeholder="10文字以内で入力" />
		            <br />
		        件名
		            <br />
		            <!-- <label for="subject"> </label>
		            <textarea name="subject" id="subject" cols="20" rows="1" class="tweet-box"><c:out value="${subject}" /></textarea> -->
		            <label for="subject"></label> <input name="subject" value="${subject}" id="subject" placeholder="30文字以内で入力" />
		            <br />
		        本文（下記に記入してください）
		            <br />
		            <label for="text"> </label>
		            <textarea name="text" id="text" cols="40" rows="10" class="tweet-box"><c:out value="${text}" /></textarea>
		            <!--<label for="text"></label> <input name="text" value="${text}" id="text" /> -->
		            <br /><br />
		            <input type="submit" class="button1" value="投稿"><br /><br />
		        </form>
		<a href="./">戻る</a> <br /> <br />
        </div>
    </body>
</html>