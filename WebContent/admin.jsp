<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>ユーザー登録情報</title>
        <link href="./css/style.css" rel="stylesheet" type="text/css">
    </head>

    <body>

            <c:if test="${ not empty errorMessages }">
            <div class="errorMessages">
            <ul>
            <c:forEach items="${errorMessages}" var="message">
                <li><c:out value="${message}" />
            </c:forEach>
            </ul>
            </div>
            <c:remove var="errorMessages" scope="session"/>
            </c:if>

        <div class="main-contents">
        <div class="header">

            <div class="header-list">
            <ul>
            <li><a href="./">ホーム</a>
      	    <li><a href="signup">ユーザー新規登録</a>
		    </ul>
		    </div>

        <br><h3>ユーザー登録情報</h3>

        </div>
        </div>

      <div class="users">

	        <c:forEach items="${users}" var="user">
	        <div style="padding: 10px; margin-bottom: 10px; border: 1px solid #333333; border-radius: 10px;">

	        <div class="user">
		        <div class="login_id-name">
		            <span class="name">名前：<c:out value="${user.name}" /></span><br />
		            <span class="login_id">ログインID：<c:out value="${user.login_id}" /></span><br />

		            <!--<span class="branch_id">支店コード：<c:out value="${user.branch_id}" /></span><br />  -->
		            <div class="branches">
			        <c:forEach items="${branches}" var="branch">
		         	<div class="branch">
				        <div class="branch-name">
					    <c:if test="${branch.id == user.branch_id}">
					        <span class="name">支店名：<c:out value="${branch.name}" /></span><br />
					    </c:if>
				        </div>
				    </div>
					</c:forEach>
				    </div>

		            <!--<span class="branch_id">支店コード：<c:out value="${user.branch_id}" /></span><br />  -->
				    <div class="positions">
				    <c:forEach items="${positions}" var="position">
				    <div class="position">
				        <div class="position-name">
					    <c:if test="${position.id == user.position_id}">
					        <span class="name">部署・役職名：<c:out value="${position.name}" /></span><br />
						</c:if>
					    </div>
					</div>
				    </c:forEach><br />

			        <script type="text/javascript">
			        function disp() {
			        // 「OK」時の処理開始 ＋ 確認ダイアログの表示
			            var res = confirm("本当によろしいですか？");
			            if (res == true) {
			            	return true;
			            }
			        // 「OK」時の処理終了
			        // 「キャンセル」時の処理開始
			            else {
			                window.alert("キャンセルされました"); // 警告ダイアログを表示
			                return false;
			            }
			        // 「キャンセル」時の処理終了
			        }
			        </script>

				    <div style="display:inline-flex">
				    <c:if test="${loginUser.branch_id == 1 && loginUser.position_id == 1}">
                    <form action="IsStopped" method="post">

                    <c:if test="${loginUser.id != user.id}">
						<c:if test="${user.is_stopped == 0}">
						    <input type="hidden" name="user_id" value="${user.id}">
						    <input type="hidden" name="is_stopped" value="${user.is_stopped}">
						    <td>
						    <input type="submit" class="button1" value="停止" onClick="return disp()"/>
						    </td>
						</c:if>
						<c:if test="${user.is_stopped == 1}">
						    <input type="hidden" name="user_id" value="${user.id}">
						    <input type="hidden" name="is_stopped" value="${user.is_stopped}">
						    <td>
					        <input type="submit" class="button1" value="復活" onClick="return disp()"/>
					        </td>
					    </c:if>
					</c:if>
				    </form>
				    </c:if>

					 <c:if test="${loginUser.id == user.id}">
					 <div class="login-now">ログイン中のユーザー</div>
					 </c:if>

					    <form action="settings" method="get">
						    <input type="hidden" name="settings" value="${user.id}">
						    <c:if test="${loginUser.branch_id == 1 && loginUser.position_id == 1}">
							<input type="submit" class="button1" value="編集" />
							</c:if>
						</form><br />
				     </div>

					 </div>
                 </div>
            </div>
            </div>
		    </c:forEach>
	    </div>
    </body>
</html>
