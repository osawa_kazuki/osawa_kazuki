<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<% pageContext.setAttribute("newLineChar", "\n"); %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>投稿一覧</title>
        <link href="./css/style.css" rel="stylesheet" type="text/css">
    </head>

    <body>

    <c:if test="${ not empty errorMessages }">
        <div class="errorMessages">
            <ul>
                <c:forEach items="${errorMessages}" var="message">
                    <li><c:out value="${message}" />
                </c:forEach>
            </ul>
        </div>
        <c:remove var="errorMessages" scope="session"/>
    </c:if>

        <div class="main-contents">
            <div class="header">

                <div class="header-list">
                <c:if test="${ empty loginUser }">
					<a href="login">ログイン</a>
				</c:if>


				<c:if test="${ not empty loginUser }">
                <ul>
      				<li><a href="./">ホーム</a>
					<li><a href="newmessage">新規投稿画面</a>

					<c:if test="${loginUser.branch_id == 1 && loginUser.position_id == 1}">
					<li><a href="admin">ユーザー管理画面</a>
                    </c:if>
					<li><a href="logout">ログアウト</a>
                </ul>
                </c:if>
                </div>

                <c:if test="${ not empty loginUser }">
                        <div class="narrow-down">
                	    <form action="./" method="get">
                	        開始
                	        <input type="date" min="2019-01-01" name="start" value="${start}" ></input>
	                	    終了 <input type="date" min="2019-01-01" name="end" value="${end}" ></input>
	                	    <input type="text" name="category" placeholder="カテゴリーを入力" value="${category}" />

	                	    <input type="submit" class="button1" value="絞り込み" />
	                	    <a href="./">
	                	    <button type="button" class="button1">リセット</button></a>
                  	    </form>
                  	    </div>
                  </c:if>

            <h2>投稿一覧</h2>

            </div>

            <div class="messages">
			    <c:forEach items="${messages}" var="message">
			    <div style="padding: 10px; margin-bottom: 10px; border: 1px solid #333333; border-radius: 10px;">

			            <div class="message">
			                <div class="login_id-name">
			                    <span class="name">名前：<c:out value="${message.name}" /></span><br />
			                    <span class="category">カテゴリー：<c:out value="${message.category}" /></span><br />
			                    <span class="subject">件名：<c:out value="${message.subject}" /></span><br />
			                </div>
			                <div class="text">本文：
			                <c:forEach var="line" items="${fn:split(message.text, newLineChar)}">
				            <c:out value="${line}" /><br/>
				            </c:forEach></div>
			                <div class="date">作成日時：<fmt:formatDate value="${message.created_date}" pattern="yyyy/MM/dd HH:mm:ss" /></div>

                            <script type="text/javascript">
			                function disp() {
				                // 「OK」時の処理開始 ＋ 確認ダイアログの表示
				                var res = confirm("本当によろしいですか？");
				                if (res == true) {
				            	    return true;
				                }
				                // 「OK」時の処理終了
				                // 「キャンセル」時の処理開始
				                else {
				                    window.alert("キャンセルされました"); // 警告ダイアログを表示
				                    return false;
				                }
				                // 「キャンセル」時の処理終了
			                }
			                </script>

                            <c:if test="${loginUser.id == message.user_id}">
	  			                <form action="DELETEMessage" method="post">
								    <input type="hidden" name="message_id" value="${message.id}">
								    <td>
								    <input type="submit" class="button1" value="新規投稿削除" onClick="return disp()" />
								    </td>
								</form><br />
                            </c:if>
			            </div>

			            <div class="comments">
					        <c:forEach items="${comments}" var="comment">
						        <c:if test="${message.id == comment.message_id}">
						            <div class="text">コメント：
						            <c:forEach var="line" items="${fn:split(comment.text, newLineChar)}">
						            <c:out value="${line}" /><br/>
						            </c:forEach></div>
						            <div class="login_id">投稿者：<c:out value="${loginUser.name}" /></div>

						            <form action="DELETEComment" method="post">
									    <input type="hidden" name="message_id" value="${comment.id}">
									    <c:if test="${loginUser.id == comment.user_id}">
									    <input type="submit" class="button1" value="コメント削除" onClick="return disp()" /><br /><br />
									    </c:if>
									</form>
								</c:if>
					        </c:forEach>

				                    <form action="newComment" method="post"><br />
					        	    <input type="hidden" placeholder="コメントを入力" name="message_id" value="${message.id}">
					                <textarea name="text" cols="75" placeholder="コメントを入力" rows="5" class="tweet-box"></textarea><br />
					                <input type="submit"class="button1" value="コメント投稿"><br />
					                </form><br />

                        </div>
                </div>
			    </c:forEach>
		    </div>
            <div class="copyright"> Copyright(c)KazukiOsawa</div>
        </div>
    </body>
</html>
