<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>ユーザー登録</title>
        <link href="./css/style.css" rel="stylesheet" type="text/css">
    </head>

	    <body>

            <div class="main-contents">
            <div class="header">

            <br><h3>ユーザー登録</h3>


            <c:if test="${ not empty errorMessages }">
	                <div class="errorMessages">
	                    <ul>
	                     <c:forEach items="${errorMessages}" var="message">
	                     <li><c:out value="${message}" />
	                     </c:forEach>
	                    </ul>
	                </div>
	                <c:remove var="errorMessages" scope="session" />
	            </c:if>

	        </div>
	        </div>

            <div class="signup">

	            <form action="signup" method="post"> <br />
	                <label for="name">名前</label> <br />
	                    <input name="name" id="name" value="${name}" /> <br />

	                <label for="login_id">ログインID</label> <br />
	                    <input name="login_id" id="login_id" value="${login_id}" /> <br />

	                <label for="password">パスワード</label> <br />
	                    <input name="password" type="password" id="password" /> <br />

	                <label for="checkPassword">パスワード（確認）</label> <br />
	                    <input name="checkPassword" type="password" id="password" /> <br />

                    <span class="name">支店<c:out value="${branch.name}" /></span><br />
	                    <select name="branch_id">
	                        <option value="0">選択してください</option>
	                        <c:forEach items="${branches}" var="branch">

	                        <c:if test="${validBranch_id == branch.id}">
							<option value="${branch.id}" selected>${branch.name}</option>
							</c:if>

							<c:if test="${validBranch_id != branch.id}">
							<option value="${branch.id}" >${branch.name}</option>
							</c:if>

							</c:forEach>
					    </select><br />

                    <span class="positon">部署・役職<c:out value="${poition.name}" /></span><br />
		                <select name="position_id">
		                    <option value="0">選択してください</option>
		                    <c:forEach items="${positions}" var="position">

	               			<c:if test="${validPosition_id == position.id}">
							<option  value="${position.id}" selected>${position.name}</option>
							</c:if>

							<c:if test="${validPosition_id != position.id}">
							<option  value="${position.id}">${position.name}</option>
							</c:if>
						    </c:forEach>
	 					</select><br /><br />

	                <input type="submit" class="button1" value="登録" /> <br />  <br /><a href="admin">戻る</a>
	            </form>

	            <div class="copyright">Copyright(c)KazukiOsawa</div>

	        </div>

	    </body>
</html>